import numpy as np
import time
import lab3_numpy.my_method_numpy as my


if __name__ == "__main__":
    n = 100
    m = 100
    ve1, ve2 = my.generate_data(n)
    matrix1, matrix2 = my.generate_matrix(n, m)

    # ----------------------------------------------------

    start1 = time.time()
    ve3 = my.multy_2_vectors(ve1, ve2)
    finish1 = time.time()
    print('dot of two vector:')
    print(f"ve3 = {ve3}")
    spend_time1 = 1000 * (finish1 - start1)
    print(f"time = {spend_time1} ")  # ms or ns

    # ------------------------------------------------------

    start2 = time.time()
    ve3_numpy = my.multy_2_vectors_numpy(ve1, ve2)
    finish2 = time.time()
    print('dot of two vector with numpy:')
    print(f"ve3_numpy = {ve3_numpy}")
    spend_time2 = 1000 * (finish2 - start2)
    print(f"time = {spend_time2}")

    # -------------------------------------------------------

    start3 = time.time()
    matrix3 = my.multy_2_matrix(matrix1, matrix2)
    finish3 = time.time()
    print('dot of two matrix:')
    print(f"matrix3 = {matrix3}")
    spend_time3 = 1000 * (finish3 - start3)
    print(f"time = {spend_time3}")

    # ------------------------------------------------------

    start4 = time.time()
    matrix3_numpy = my.multy_2_matrix_numpy(matrix1, matrix2)
    finish4 = time.time()
    print('dot of two matrix with numpy')
    print(f"matrix3_numpy={matrix3_numpy}")
    spend_time4 = 1000 * (finish4 - start4)
    print(f"time = {spend_time4}")

    # ------------------------------------------------------

    start5 = time.time()
    vec_mat = my.multy_vect_matr(ve1, matrix1)
    finish5 = time.time()
    print("dot of matrix & vector")
    print('vec_mat', vec_mat)
    spend_time5 = 1000 * (finish5 - start5)
    print(f"time = {spend_time5}")

    # -------------------------------------------------------

    start6 = time.time()
    vec_mat_numpy = my.multy_vect_matr_numpy(ve1, matrix1)
    finish6 = time.time()
    print("dot of matrix & vector with numpy")
    print('vec_mat_numpy', vec_mat_numpy)
    spend_time6 = 1000 * (finish6 - start6)
    print(f"time = {spend_time6}")